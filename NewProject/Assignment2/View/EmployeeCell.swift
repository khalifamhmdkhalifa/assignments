//
//  EmployeeCellTableViewCell.swift
//  NewProject
//
//  Created by khalifa on 7/23/20.
//

import UIKit
import SnapKit

class EmployeeCell: UITableViewCell {
    private var nameLabel: UILabel {
        didSet {
            nameLabel.textAlignment = .natural
        }
    }
    private var birthYearLabel: UILabel {
        didSet {
            birthYearLabel.textAlignment = .natural
        }
    }
    private var salaryLabel: UILabel {
        didSet {
            salaryLabel.textAlignment = .natural
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        let nameLabel = UILabel()
        let birthYearLabel = UILabel()
        let salaryLabel = UILabel()
        self.nameLabel = nameLabel
        self.birthYearLabel = birthYearLabel
        self.salaryLabel = salaryLabel
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(nameLabel)
        contentView.addSubview(birthYearLabel)
        contentView.addSubview(salaryLabel)
        addViewConstraints()
    }
    
    func addViewConstraints() {
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView.safeAreaLayoutGuide.snp.top).offset(16)
            make.leading.equalTo(contentView.safeAreaLayoutGuide.snp.leading).offset(16)
        }
        birthYearLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(5)
            make.leading.equalTo(nameLabel.snp.leading)
        }
        salaryLabel.snp.makeConstraints { make in
            make.top.equalTo(birthYearLabel.snp.bottom).offset(14)
            make.leading.equalTo(nameLabel.snp.leading)
            make.bottom.equalTo(contentView.safeAreaLayoutGuide.snp.bottom).offset(-16)
        }
    }
    
    func configure(_ model: Employee) {
        self.nameLabel.text = model.name ?? ""
        if let birthYear = model.birthYear {
            self.birthYearLabel.text = "Birth Year: \(birthYear)"
        } else {
            self.birthYearLabel.text = ""
        }
        if let salary = model.formatedSalary() {
            self.salaryLabel.text = "Salary: \(salary)"
        } else {
            self.salaryLabel.text = ""
        }
    }
    
}

extension UITableViewCell {
    class var reuseIdentifier: String { "\(self)" }
}
