//
//  ViewController.swift
//  NewProject
//
//  Created by khalifa on 7/23/20.
//

import UIKit
import SnapKit
class ViewController: UIViewController {
    private weak var employeesTable: UITableView! {
        didSet {
            employeesTable.dataSource = self
            employeesTable.allowsSelection = false
            employeesTable.register(EmployeeCell.self, forCellReuseIdentifier: EmployeeCell.reuseIdentifier)
        }
    }
    // should be injected from some where outside this class
    var employeesDirectory: EmployeeDirectoryProtocol = EmployeeDirectory()
    private var employees: [Employee] {
        return employeesDirectory.employees
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.employeesTable = createEmployeesTable()
        self.title = "Employees"
        NotificationCenter.default.addObserver(forName: .kEmployeeDirectoryDidUpdateNotification, object: nil, queue: .main, using: { [weak self] _ in
            self?.employeesTable.reloadData()
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        employeesDirectory.update()
        addSortButton()
    }
    
    private func createEmployeesTable() -> UITableView {
        let table = UITableView()
        self.view.addSubview(table)
        table.snp.makeConstraints { make in
            guard let superview = table.superview else { return }
            make.top.equalToSuperview().offset(8)
            make.leading.equalTo(superview.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(superview.safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(superview.safeAreaLayoutGuide.snp.bottom)
        }
        return table
    }
    
    private func addSortButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "sort", style: .plain, target: self, action: #selector(didTapSort))
    }
    
    @objc func didTapSort() {
        employeesDirectory.sortEmployeesIfNeeded() { [weak self] in
            self?.employeesTable.reloadData()
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeCell.reuseIdentifier, for: indexPath) as? EmployeeCell {
            cell.configure(employees[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}

extension ViewController: CustomDialogDelegate {
    func didTapConfirm() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func didTapCancel() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
