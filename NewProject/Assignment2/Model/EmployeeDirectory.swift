//
//  EmployeeDirectory.swift
//  NewProject
//
//  Created by khalifa on 7/23/20.
//

import Foundation

import UIKit

class EmployeeDirectory: NSObject {
    var employees: [Employee] = []
    var isUpdating = false
    private var isSorted = false
    
    private func doUpdateInBackground() {
        Thread.sleep(forTimeInterval: 2)
        isSorted = false
        let names = ["Anne", "Lucas", "Marc", "Zeus", "Hermes", "Bart", "Paul",
                     "John", "Ringo", "Dave", "Taylor"]
        let surnames = ["Hawkins", "Simpson", "Lennon", "Grohl", "Hawkins",
                        "Jacobs", "Holmes", "Mercury", "Matthews"]
        let amount = names.count * surnames.count
        var employees: [Employee] = []
        for _ in 0..<amount {
            let fullName = String(format: "%@ %@", names[Int(arc4random()) %
                names.count], surnames[Int(arc4random()) % surnames.count])
            employees.append(Employee(with: fullName, birthYear: 1997 -
                Int(arc4random()) % 50))
        }
        DispatchQueue.global(qos: .utility).async {
            self.updateDidFinishWithResults(results: employees)
        }
    }
    
    private func updateDidFinishWithResults(results: [Employee]) {
        employees = results
        isUpdating = false
        NotificationCenter.default.post(name: NSNotification.Name.kEmployeeDirectoryDidUpdateNotification, object: self)
    }
}

extension EmployeeDirectory: EmployeeDirectoryProtocol {
    func sortEmployeesIfNeeded(completion: @escaping () -> Void ) {
        guard !isSorted, !employees.isEmpty else {
            DispatchQueue.main.async { completion() }
            return
        }
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            guard let self = self else { return }
            self.employees.sort {
                guard let name1 = $0.name , let name2 = $1.name else { return false }
                return name1 < name2
            }
            self.isSorted = true
            DispatchQueue.main.async {
                completion()
            }
        }
    }
    
    func update() {
        guard !isUpdating else { return }
        isUpdating = true
        self.doUpdateInBackground()
    }
}

protocol EmployeeDirectoryProtocol {
    var employees: [Employee] { get }
    func update()
    func sortEmployeesIfNeeded(completion: @escaping ()->Void)
}

extension EmployeeDirectoryProtocol {
    static var kEmployeeDirectoryDidUpdateNotification: String { "kEmployeeDirectoryDidUpdateNotification"
    }
}

extension NSNotification.Name {
    static let kEmployeeDirectoryDidUpdateNotification = Notification.Name(EmployeeDirectory.kEmployeeDirectoryDidUpdateNotification)
}
