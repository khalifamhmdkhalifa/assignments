//
//  Employee.swift
//  NewProject
//
//  Created by khalifa on 7/23/20.
//

import Foundation

class Employee: NSObject {
    let startingSalary: Double = 10000
    var name: String?
    var birthYear: Int?
    var salary: Double?
    
    init(with name: String, birthYear: Int) {
        self.name = name
        self.birthYear = birthYear
        salary = startingSalary
    }
    
    func formatedSalary() -> String? {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        guard let salary = salary else { return "" }
        return formatter.string(from: salary as NSNumber) ?? ""
    }
}
