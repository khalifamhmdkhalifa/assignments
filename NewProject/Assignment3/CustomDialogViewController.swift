//
//  CustomDialogViewController.swift
//  NewProject
//
//  Created by khalifa on 7/24/20.
//

import UIKit

class CustomDialogViewController: UIViewController {
    @IBOutlet weak var messagLabel: UILabel! {
        didSet {
            messagLabel.text = message
        }
    }
    @IBOutlet weak var confirmButton: UIButton! {
        didSet {
            if let confirmButtonText = confirmButtonText {
                confirmButton.setTitle(confirmButtonText, for: .normal)
            } else {
                confirmButton.isHidden = true
            }
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            if let cancelButtonText = cancelButtonText {
                cancelButton.setTitle(cancelButtonText, for: .normal)
            } else {
                cancelButton.isHidden = true
            }
            cancelButton.layer.borderColor = UIColor.init(red: 1.0/255.0, green: 181.0/255.0, blue: 169.0/255.0, alpha: 1).cgColor
            cancelButton.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var containerView: UIView!
    weak var delegate: CustomDialogDelegate?
    var message: String?
    var confirmButtonText: String?
    var cancelButtonText: String?
    
    static func createDialog(message: String, confirmButtonText: String? = nil, cancelButtonText: String? = nil, delegate: CustomDialogDelegate?) -> CustomDialogViewController? {
        guard let viewController = UIStoryboard(name: "CustomDialogViewController", bundle: nil).instantiateViewController(withIdentifier: "CustomDialogViewController") as? CustomDialogViewController else { return nil }
        viewController.delegate = delegate
        viewController.cancelButtonText = cancelButtonText
        viewController.confirmButtonText = confirmButtonText
        viewController.message = message
        return viewController
    }
    
    static func showDialog(in viewController: UIViewController, message: String, confirmButtonText: String? = nil, cancelButtonText: String? = nil, delegate: CustomDialogDelegate?) {
        guard let dialog = createDialog(message: message, confirmButtonText: confirmButtonText, cancelButtonText: cancelButtonText, delegate: delegate) else { return }
        dialog.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        dialog.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.present(dialog, animated: true, completion: nil)
    }
    
    
    @IBAction func didTapConfirm(_ sender: Any) {
        delegate?.didTapConfirm()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.dropShadow()
    }
    
    @IBAction func didTapCancel(_ sender: Any) {
        delegate?.didTapCancel()
    }
}

protocol CustomDialogDelegate: class {
    func didTapConfirm()
    func didTapCancel()
}
